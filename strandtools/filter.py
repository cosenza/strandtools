import numpy as np
import pandas as pd
from tqdm import tqdm

# visualization
import matplotlib.pyplot as plt
import seaborn as sns
import colorcet as ccet


def get_consensus(cnc):
    X = cnc if isinstance(cnc, pd.DataFrame) else cnc.counts
    consensus = X.groupby(['chrom', 'start'])['pred'].agg(lambda x: x.mode().iloc[0]).reset_index()
    #consensus = cnc.counts.groupby(['chrom', 'start'])['pred'].agg(lambda x: x.value_counts().index.values[0]).reset_index()
    consensus.rename({'pred': 'consensus'}, axis='columns', inplace=True)
    return consensus


# Consensus score is deprecated
# def get_consensus_score(cnc):
#     consensus = cnc.counts.groupby(['chrom', 'start'])['pred'].agg(lambda x: x.value_counts().index.values[0]).reset_index()
#     consensus.rename({'pred': 'consensus'}, axis='columns', inplace=True)
#     score = pd.merge(cnc.counts[['chrom', 'start', 'pred', 'segm_id_1']], consensus, on=['chrom', 'start'], how='left')
#     score['consensus_score'] = score['pred'] == score['consensus'] 
#     score = score.groupby('segm_id_1')['consensus_score'].agg(lambda x: x.sum() / x.shape[0]).reset_index()
#     return consensus, score


def get_probability_thresholds(cnc, n_bins=256, stringency=None, plot=False):
    '''Probability thresholding strategy based on elbow point estimation on the probability histogram.
    Each classifier-prediction combination has its own specific probability distribution histogram, usually with L shape.
    Finding the elbow point of the probability histogram allows to estimate of an optimal detection threshold for the predictions.
    From the elbow point, a second point is calculated with a modification of the kneedle algorithm, which represent the most lenient threshold.
    A stringency factor allows to modulate the proability threshold between the detected elbow-primary and secondary point.'''

    stringency = 0.5 if stringency is None else stringency

    hist_data = cnc.counts_merged.groupby(['classifier', 'pred'])['prob'].agg(lambda x: x.to_list()).reset_index()
    
    if plot:
        fig, ax = plt.subplots(figsize=(2, 2*hist_data.shape[0]), nrows=hist_data.shape[0])

    thresholds = []

    for n, row in tqdm(hist_data.iterrows()):
        h, bins = np.histogram(row['prob'], bins=np.linspace(0,1,n_bins))
        maxy = np.max(h)
        maxx = np.argmax(h)
        h = h/maxy

        x1, y1 = (0, h[0]) # xy
        x2, y2 = (np.argmax(h), np.max(h)) # xy
        # transform coordinates
        bin_norm_factor = bins[x2]
        bins = bins[:x2+1] / bin_norm_factor
        h = h[:x2+1]
        x2, y2 = (bins[np.argmax(h)], np.max(h))
        
        # find most distant point
        dd = []
        for x0, y0 in zip(bins, h):
            d = np.abs((x2-x1)*(y1-y0) - (x1-x0)*(y2-y1)) / np.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))
            dd.append(d)
        
        x3, y3 = bins[np.argmax(dd)], h[np.argmax(dd)]
        
        # perpendicular line -> opposite reciprocal slope
        m = (y2-y1)/(x2-x1)
        b = y2 - m*x2 
        mp = -1/m # perpendicular slope
        bp = y3 -mp*x3 # intercept
        
        # cross point
        x4 = (bp - b) / (m - mp)
        y4 = m*x4 + b
        
        # calculate threshold
        tmin, tmax = x4*bin_norm_factor, x3*bin_norm_factor
        t = (tmax-tmin)*stringency + tmin
        thresholds.append(t)
        
        if plot:
            sns.lineplot(x=bins, y=h, ax=ax[n])
            sns.lineplot(x=bins, y=bins*m+b, ax=ax[n])
            sns.lineplot(x=bins, y=bins*mp+bp, ax=ax[n])
            sns.scatterplot(x=[x3,x4], y=[y3,y4], color='red', ax=ax[n])
            sns.scatterplot(x=[x1,x2], y=[y1,y2], color='orange', ax=ax[n])
            # formatting
            ax[n].axvline((x3-x4)*stringency+x4, linestyle='--', color = 'gray')
            ax[n].set_xlim(-0.1,1.1)
            ax[n].set_ylim(-0.1,1.1)
            ax[n].set_title('{} - {}'.format(row['classifier'], row['pred']))
        
    if plot:
        fig.tight_layout()

    hist_data['prob_threshold'] = thresholds

    return hist_data.loc[:, ['classifier', 'pred', 'prob_threshold']]


def get_prob_pass(x, probability_thresholds):
    sel = (probability_thresholds['classifier'] == x['classifier']) & (probability_thresholds['pred'] == x['pred'])
    return x['prob'] >= probability_thresholds.loc[sel, 'prob_threshold'].values[0]


def probability_pass(cnc, n_bins=256, stringency=None, plot=False):
    thresholds = get_probability_thresholds(cnc, n_bins=n_bins, stringency=stringency, plot=plot)
    prob_pass = cnc.counts_merged.apply(lambda x: get_prob_pass(x,thresholds), axis=1)
    return prob_pass


def exclusion_score(x, exclusion_bed):
    
    for n,row in exclusion_bed[exclusion_bed['chrom'] == x['chrom']].iterrows():
        end = np.min([row['end'], x['end']]) 
        start = np.max([row['start'], x['start']])
        if end-start > 0:
            return (end-start) / (x['end'] - x['start'])
        else:
            continue
    return 0


def exclusion_pass(cnc, exclusion_bed, threshold=.3):
    exclusion_bed = pd.read_csv(exclusion_bed, sep='\t', 
                                header=None, names=['chrom', 'start', 'end', 'band'])
    scores =  cnc.counts_merged.apply(lambda x: exclusion_score(x, exclusion_bed), axis=1)    
    return scores <= threshold


def low_count_pass(cnc, score_threshold=.05, segm_id_col=None):
    segm_id_col = 'super_segm_id' if cnc._supersegments else 'segm_id_1'

    lcb = cnc.counts.groupby(segm_id_col)['tot_count'].agg(lambda x: (x <= 3).sum()).reset_index()
    lcb.rename({'tot_count': 'low_count_bins'}, axis='columns', inplace=True)

    ecb = cnc.counts.groupby(segm_id_col)['excluded'].sum().reset_index()
    ecb.rename({'excluded': 'excluded_count_bins'}, axis='columns', inplace=True)

    cnc.counts_merged = pd.merge(cnc.counts_merged, lcb, on=segm_id_col, how='left')
    cnc.counts_merged = pd.merge(cnc.counts_merged, ecb, on=segm_id_col, how='left')

    if 'n_bins' not in cnc.counts_merged.columns.to_list():
        nbins = cnc.counts.groupby(segm_id_col)['tot_count'].count().reset_index()
        nbins.rename({'tot_count': 'n_bins'}, axis='columns', inplace=True)
        cnc.counts_merged = pd.merge(cnc.counts_merged, nbins, on=segm_id_col, how='left')

    l = cnc.counts_merged['low_count_bins']
    e = cnc.counts_merged['excluded_count_bins']
    n = cnc.counts_merged['n_bins']
    cnc.counts_merged['low_count_score'] = (l - (l&e)) / n  

    # low count score does not apply to copy number 1
    return (cnc.counts_merged['low_count_score'] <= score_threshold) | (cnc.counts_merged['pred'] == 1)


def cn1_pass(cnc):
    cnc.counts_merged['is_WC'] = cnc.counts_merged['wf'].between(0.1,0.9)
    out = ~(
        (cnc.counts_merged['pred']==1) & (cnc.counts_merged['is_WC'])
        )
    return out


def n_segm_pass(cnc, threshold, ylim=None):
    n_segms = cnc.counts_merged.groupby('cell')['tot_count'].count().sort_values()
    segm_pass = n_segms[n_segms <= threshold].index.to_list()
    fig, ax = plt.subplots(figsize=(3,3))
    sns.stripplot(n_segms, ax=ax)
    ax.set_ylabel('number of segments')
    ax.axhline(threshold, linestyle='--', color='gray')
    if ylim is not None:
        ax.set_ylim(ylim[0],ylim[1])
    n_segms = cnc.counts_merged.groupby('cell')['tot_count'].count().sort_values()
    segm_pass = n_segms[n_segms <= threshold].index.to_list()
    return cnc.counts_merged['cell'].isin(segm_pass)


def noise_pass(cnc, cv_threshold=0.5, stderr_threshold=1.5, plot=False):
    cnc.counts_merged['tot_count_cv'] = cnc.counts_merged['tot_count_std'] / cnc.counts_merged['tot_count']
    cnc.counts_merged['tot_count_stderr'] = cnc.counts_merged['tot_count_std'] / np.sqrt(cnc.counts_merged['n_bins'])

    if plot:
        fig, ax = plt.subplots(figsize=(6,3), ncols=2, sharey=True, sharex=True)
        sns.scatterplot(data=cnc.counts_merged, x='tot_count', y='n_bins',
                        hue='tot_count_cv', palette=ccet.cm.rainbow, hue_norm=(0,2),
                        alpha=.3, 
                        marker='.', linewidth=0, ax=ax[0])
        #ax.set_ylim(0,5)
        ax[0].set_yscale('log')
        ax[0].set_xscale('log')
        ax[0].axhline(12, linestyle='--', color='gray')
        ax[0].axhline(32, linestyle='--', color='gray')

        sns.scatterplot(data=cnc.counts_merged[cnc.counts_merged['tot_count_cv']<=0.5], x='tot_count', y='n_bins',
                        hue='tot_count_stderr', palette=ccet.cm.rainbow, 
                        alpha=.3, 
                        marker='.', linewidth=0, ax=ax[1])
        ax[1].axhline(12, linestyle='--', color='gray')
        ax[1].axhline(32, linestyle='--', color='gray')
        ax[1].set_yscale('log')
        ax[1].set_xscale('log')

    return (cnc.counts_merged['tot_count_cv'] <= cv_threshold)|(cnc.counts_merged['tot_count_stderr'] <= stderr_threshold)


def qc_pass(cnc, 
            probability_pass_stringency,
            exclusion_bed, 
            n_segm_threshold,
            exclusion_score_threshold,
            low_count_threshold,
            cv_threshold,
            stderr_threshold,
            ylim=None):
    
    cnc.counts_merged['prob_pass'] = probability_pass(cnc, stringency=probability_pass_stringency, plot=True)
    cnc.counts_merged['exclusion_pass'] = exclusion_pass(cnc, exclusion_bed=exclusion_bed, threshold=exclusion_score_threshold)
    cnc.counts_merged['low_count_pass'] = low_count_pass(cnc, score_threshold=low_count_threshold)
    cnc.counts_merged['cn1_pass'] = cn1_pass(cnc)
    cnc.counts_merged['n_segm_pass'] = n_segm_pass(cnc, threshold=n_segm_threshold, ylim=ylim)
    cnc.counts_merged['noise_pass'] = noise_pass(cnc, cv_threshold=cv_threshold, stderr_threshold=stderr_threshold)
    
    qc_filter = (cnc.counts_merged['exclusion_pass']) &\
            (cnc.counts_merged['low_count_pass']) &\
            (cnc.counts_merged['prob_pass']) &\
            (cnc.counts_merged['cn1_pass']) &\
            (cnc.counts_merged['pred'] >= 1) &\
            (cnc.counts_merged['noise_pass']) &\
            (cnc.counts_merged['n_segm_pass'])

    cnc.counts_merged['pred_QC_pass'] = qc_filter

    return cnc


def get_changepoints(cnc, min_segm_size=6, segm_id_col=None):
    segm_id_col = 'super_segm_id' if cnc._supersegments else 'segm_id_1'

    cm = cnc.counts_merged.copy()
    cm = cm.loc[cm['n_bins'] >= min_segm_size, :]

    wc_ratios = []
    for cn in cm['pred'].sort_values().unique():
        if cn <= 0:
            continue
        for x in np.arange(0, cn+1):
            wc_ratios.append(pd.Series({
                'pred': cn,
                'ratio': np.round(x/(cn), decimals=2),
                'ratio_key': int('{}{}'.format(x,cn-x))
            }))
    wc_ratios = pd.DataFrame(wc_ratios)
    wc_ratios = pd.concat([wc_ratios, pd.Series({'pred': -1, 'ratio': 0, 'ratio_key': -1}).to_frame().T], ignore_index=True, axis=0)

    def clip_ratio(x):
        return wc_ratios[wc_ratios['pred']==x['pred']].iloc[(wc_ratios.loc[wc_ratios['pred']==x['pred'], 'ratio']-x['wf']).abs().argmin()]['ratio_key']
    
    cm['wf'].fillna(0, inplace=True)
    cm['ratio_key'] = cm.apply(clip_ratio, axis=1)

    cm['pred_shift'] = cm.groupby(['chrom', 'cell'])['pred'].shift(1)
    cm['ratio_key_shift'] = cm.groupby(['chrom', 'cell'])['ratio_key'].shift(1)

    cm['changepoint_pred'] = (cm['pred'] != cm['pred_shift'])
    cm['changepoint_ratio'] = (cm['ratio_key'] != cm['ratio_key_shift']) # strand
    cm['changepoint_SCE'] = (cm['changepoint_pred'] ^ cm['changepoint_ratio'])

    cnc.counts_merged = pd.merge(cnc.counts_merged, cm[[segm_id_col, 'changepoint_pred', 'changepoint_ratio', 'changepoint_SCE']], on=segm_id_col, how='left')

    cnc.counts_merged['changepoint_pred'].fillna(False, inplace=True)
    cnc.counts_merged['changepoint_ratio'].fillna(False, inplace=True)
    cnc.counts_merged['changepoint_SCE'].fillna(False, inplace=True)

    return cnc


def get_recurrency_score(cnc):
    
    recurrency_mat = pd.pivot_table(cnc.counts, index=['chrom', 'start'], values='pred', columns='cell')

    def recurrency_score(row):
        ref = recurrency_mat.loc[(row['chrom'], row['start']):(row['chrom'], row['end']-1), row['cell']]
        
        roi = recurrency_mat.loc[(row['chrom'], row['start']):(row['chrom'], row['end']-1), :]
        
        return roi.apply(lambda x: (x==ref).sum()/ref.shape[0], axis=0).sum()
        
    scores = []
    for n, row in tqdm(cnc.counts_merged.iterrows()):
        s = recurrency_score(row)
        scores.append(s)
    
    return scores    