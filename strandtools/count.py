import argparse
import pysam
from glob import glob 
import numpy as np
import pandas as pd
import multiprocessing as mp
from time import time

def check_read(r):
    return not (r.is_secondary | r.is_supplementary | r.is_qcfail | r.is_duplicate | (r.mapping_quality <= 10) | r.is_read2)

def count_w(r):
    return check_read(r) & (not r.is_forward)

def count_c(r):
    return check_read(r) & r.is_forward

def count_wh1(r):
    if r.has_tag('HP'):
        return count_w(r) & (r.get_tag('HP')==1)
    else:
        return False
def count_wh2(r):
    if r.has_tag('HP'):
        return count_w(r) & (r.get_tag('HP')==2)
    else:
        return False

def count_ch1(r):
    if r.has_tag('HP'):
        return count_c(r) & (r.get_tag('HP')==1)
    else:
        return False
def count_ch2(r):
    if r.has_tag('HP'):
        return count_c(r) & (r.get_tag('HP')==2)
    else:
        return False

def main(n_cpus=None):
    pool = mp.Pool(n_cpus)
    args = [[filepath, BINSIZE] for filepath in FILEPATHS]
    results = pool.map(_count_reads, args)
    return results
        
def _count_reads(arg_list):
    filepath, binsize = arg_list
    return count_reads(filepath, binsize)

def count_reads(filepath, binsize):
    samfile = pysam.AlignmentFile(filepath, "rb")

    # get metadata
    # get sample and cell id
    if 'RG' in samfile.header.keys():
        rg = samfile.header.get('RG')
        if len(rg) > 1:
            raise ValueError('more than one cell in bam file') 
        cell_id = rg[0]['ID']
        sample = rg[0]['SM']
    else:
        raise RuntimeError('RG tag with cell and sample id is missing in bam header')

    # get chromosome lengths
    # needed to correct end coordinate on the last chromosome bin
    if 'SQ' in samfile.header.keys():
        chroms = samfile.header.get('SQ')
        kk = []
        vv = []
        for ch in chroms:
            if ch['SN'][3:] in list(map(str, range(1,23))) + ['X', 'Y']:
                kk.append(ch['SN'])
                vv.append(ch['LN'])

        chr_lenghts = pd.Series(vv, index=kk)
    else:
        raise RuntimeError('SQ tag with chromosome length information is missing in bam header')

    # counting loop per chromosome
    counts = []
    for ch in chr_lenghts.index:

        len_chrom = chr_lenghts[ch]
        i = 0
        while True:
            funcs = [count_w, count_c, count_wh1, count_wh2, count_ch1, count_ch2]
            cc = []
            for f in funcs:
                cc.append(samfile.count(contig=ch, start=i, stop=min(len_chrom, i+binsize), read_callback=f))
            #counts.append([ch, i, i+bin_size, w, c])
            counts.append([cell_id, sample, ch, i, min(len_chrom-1, i+binsize), *cc])              
            i += binsize
            if i > len_chrom:
                break


    counts = pd.DataFrame(counts, columns=['cell', 'sample', 'chrom', 'start', 'end', 'w', 'c', 'wh1', 'wh2', 'ch1', 'ch2'])

    # assigning strand-state class
    counts['tot_count'] = counts['w'] + counts['c']
    counts['wf'] = np.nan
    # smoothening class assignment to account for data noise
    counts.loc[counts['tot_count']>=5, 'wf'] = counts['w'].rolling(10, min_periods=5).median() / counts['tot_count'].rolling(10, min_periods=5).median()
    counts['wf'] = counts['wf'].interpolate('linear', limit=10)
    counts['class'] = 'None'
    counts.loc[counts['wf'] < .1, 'class'] = 'CC'
    counts.loc[counts['wf'] >= .9, 'class'] = 'WW'
    counts.loc[(counts['wf'] >= .1)&(counts['wf'] < .9), 'class'] = 'WC'

    return counts.loc[:,['cell', 'sample', 'chrom', 'start', 'end', 'w', 'c', 'class', 'wh1', 'wh2', 'ch1', 'ch2']]


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    
    ap.add_argument('-i', '--input', required=True, help='path to bam directory')
    ap.add_argument('-e', '--ext', required=True, help='file extension')
    ap.add_argument('-b', '--binsize', required=False, help='bin size in bps')
    ap.add_argument('-n', '--ncpus', required=False, help='number of cpus')
    ap.add_argument('-o', '--output', required=True, help='output filepath (path and filename)')

    args = vars(ap.parse_args())
    
    COUNTDIR = args['input']
    EXT = args['ext']
    BINSIZE = int(args['binsize']) if args['binsize'] is not None else 200000
    N_CPUS = int(args['ncpus']) if args['ncpus'] is not None else (mp.cpu_count() -1)
    SAVEPATH = args['output']

    if EXT[0] != '.':
        EXT = '.'+EXT


    FILEPATHS = glob(COUNTDIR+'*'+EXT)

    print('processing directory:')
    print(COUNTDIR)
    print('files detected: {}'.format(len(FILEPATHS)))
    print('output file:')
    print(SAVEPATH)

    t0 = time()
    output = main(n_cpus=N_CPUS)
    t1 = time()
    print('execution time (min): {:.3f}'.format((t1-t0)/60))
    print('saving ...')
    out = []
    for o in output:
        if isinstance(o, pd.core.frame.DataFrame):
            out.append(o)
        else:
            continue

    counts = pd.concat(out)
    counts.to_csv(SAVEPATH, sep='\t', index=False)
    


