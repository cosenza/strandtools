import pandas as pd
from dash import Dash, dcc, html, Input, Output, callback
import plotly.express as px



class StrandSeqViewer():
    def __init__(self, data=None):
        '''Interactive StrandSeq plot visualization with plotly and Dash.
        Selecting a segment will display some statistics. The data concerning the segment is also stored in the table accessible via self.stats'''

        self.app = Dash('StrandSeqViewer')
        self.fig = None
        self.stats = pd.DataFrame()
        self._sel_count = 0
        self.chr_id = None

        if data is not None:
            if data['chrom'].unique().shape[0] != 1:
                raise ValueError('data must contain only one chromosome')
            if data['cell'].unique().shape[0] != 1:
                raise ValueError('data must contain only one cell')
            self.chr_id = data['cell'].unique()[0] +"_"+ data['chrom'].unique()[0] 
            self.data = self.melt_data(data)
            self.view()
        else:
            self.data = None
            
        
    def init_layout(self):
        self.app.layout = html.Div([
            dcc.Graph(
                id='basic-interactions',
                figure=self.fig
            ),

            html.Div(className='row', children=[
                html.Div([
                    dcc.Markdown("""Selection data"""),
                    html.Pre(id='selected-data'),
                ])
            ])
        ])

        @callback(
            Output('selected-data', 'children'),
            Input('basic-interactions', 'selectedData'))
        def display_selected_data(selectedData):
            try:
                start, end = selectedData['range']['x']
                out = self.calc_stats(start, end)
                out = pd.Series(out, name=out['id'])
                self.stats = pd.concat([self.stats, out], axis=1)
                self._sel_count += 1
                out = out.to_string(float_format=lambda x: "{:.4f}".format(x))
                return out
            except:
                # exception needed if nothing is selected
                return ''   
            
    def melt_data(self, data):
        '''Convert StrandSeq table to long format'''
        data = data.loc[:, ['chrom', 'start', 'end', 'w', 'c', 'tot_count']]
        data['c'] = data['c']*-1
        return pd.melt(data, id_vars=['chrom', 'start'], value_vars=['w','c', 'tot_count'], var_name='strand', value_name='count')
        
    def view(self, data=None):
        '''Launch Dash app and view data'''
        if data is not None:
            self.chr_id = data['cell'].unique()[0] +"_"+ data['chrom'].unique()[0] 
            self.data = self.melt_data(data)
            
        self.fig = px.bar(self.data.loc[self.data['strand'].isin(['w','c']), :], x='start', y='count', color='strand',
                   color_discrete_sequence=['#f4a560', '#668b8b']) # sandybrown, paleturquoise4

        self.fig.update_traces(marker_line_width = 0,
                          selector=dict(type="bar"))
        self.fig.update_layout(clickmode='event+select', dragmode='select')

        self.init_layout()
        self.app.run(debug=True)
    
    def calc_stats(self, start, end):
        '''Calculate segment statistic between start and end coordinates'''
        segm = self.data[(self.data['start']>=start)&(self.data['start']<=end)].fillna(0)
        stats = {
            'id': self.chr_id,
            'segm_id': self._sel_count,
            'start': start,
            'end': end,
            'size': end-start,
            'w_fraction': (segm.loc[segm['strand']=='w', 'count']).sum() / (segm.loc[segm['strand']=='tot_count', 'count']).sum(),
            'w_mean': segm.loc[segm['strand']=='w', 'count'].mean(),
            'c_mean': segm.loc[segm['strand']=='c', 'count'].abs().mean(),
            'tot_count_mean': segm.loc[segm['strand']=='tot_count', 'count'].mean()
        }
        return stats